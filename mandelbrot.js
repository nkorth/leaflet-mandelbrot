"use strict";

var mandelbrotLayer = L.tileLayer.canvas({
    continuousWorld: true,
    async: true,
    maxZoom: 99
});
var tiles = {};
var worker = new Worker("mandelbrot_worker.js");

mandelbrotLayer.drawTile = function(canvas, pos, zoom){
    worker.postMessage({
        pos: pos,
        zoom: zoom
    });
    tiles[pos.x+","+pos.y+","+zoom] = canvas;
};
worker.onmessage = function(e){
    var canvas = tiles[e.data.pos.x+","+e.data.pos.y+","+e.data.zoom];
    var ctx = canvas.getContext('2d');
    var i = new Image();
    i.src = e.data.image;
    i.onload = function(){
        // ...yep >_<
        ctx.drawImage(i, 0, 0);
    };
    mandelbrotLayer.tileDrawn(canvas);
    delete tiles[e.data.pos.x+","+e.data.pos.y+","+e.data.zoom];
};

var map = L.map("map_container", {
    center: [128, -256],
    zoom: 0,
    layers: [mandelbrotLayer],
});
