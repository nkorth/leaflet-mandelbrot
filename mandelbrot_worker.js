"use strict";

importScripts("pnglib.js?2");

function mandelbrotPoint(x, y, iter_max){
    var za = 0;
    var zb = 0;
    for(var i = 0; i < iter_max; i++){
        var nza = za*za - zb*zb + x;
        var nzb = 2*za*zb + y;
        if(nza*nza + nzb*nzb >= 4) break;
        za = nza;
        zb = nzb;
    }
    return i / iter_max;
}

function hsl2rgb(h, s, l) {
    var v, min, sv, sextant, fract, vsf;

    if (l <= 0.5) v = l * (1 + s);
    else v = l + s - l * s;

    if (v === 0) return [0, 0, 0];
    else {
        min = 2 * l - v;
        sv = (v - min) / v;
        h = 6 * h;
        sextant = Math.floor(h);
        fract = h - sextant;
        vsf = v * sv * fract;
        if (sextant === 0 || sextant === 6) return [v, min + vsf, min];
        else if (sextant === 1) return [v - vsf, v, min];
        else if (sextant === 2) return [min, v, min + vsf];
        else if (sextant === 3) return [min, v - vsf, v];
        else if (sextant === 4) return [min + vsf, min, v];
        else return [v, min, v - vsf];
    }
}

self.onmessage = function(e){
    var tx = e.data.pos.x,
        ty = e.data.pos.y,
        zoom = e.data.zoom;
    var itermax = Math.floor(15 + 0.6*zoom*zoom);

    var depth = 256; // color depth
    var image = new PNGlib(256, 256, depth);
    for(var y = 0; y < 256; y++){
        for(var x = 0; x < 256; x++){
            var mx = (x/256 + tx) / Math.pow(2, zoom-1);
            var my = (y/256 + ty) / Math.pow(2, zoom-1);
            var shade = mandelbrotPoint(mx, my, itermax);
            var rgb = hsl2rgb(0.61-0.1*shade, 1, 0.5-Math.abs(shade-0.5));
            rgb[0] = Math.floor(rgb[0] * (depth-1));
            rgb[1] = Math.floor(rgb[1] * (depth-1));
            rgb[2] = Math.floor(rgb[2] * (depth-1));
            image.buffer[image.index(x, y)] = image.color(rgb[0], rgb[1], rgb[2]);
        }
    }
    self.postMessage({
        pos: e.data.pos,
        zoom: e.data.zoom,
        image: 'data:image/png;base64,'+image.getBase64()
    });
};
